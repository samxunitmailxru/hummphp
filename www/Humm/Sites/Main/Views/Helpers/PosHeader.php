
<?php if (!$this) { exit; } ?>

</head>
 <body>

  <div class="container">
   <div class="header clearfix">
    <nav>
     <ul class="nav nav-pills float-right">
      <li class="nav-item">
       <a class="nav-link" href="<?= $siteUrl ?>?home">Home</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="<?= $siteUrl ?>?about">About</a>
      </li>
     </ul>
    </nav>
    <h3 class="text-muted"><a href="https://www.davidesperalta.com" title="www.davidesperalta.com">Humm PHP</a></h3>
   </div>