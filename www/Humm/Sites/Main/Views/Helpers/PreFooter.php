
<?php if (!$this) { exit; } ?>

 <footer class="footer text-center">
  <p>&copy; 2019 DecSoft's Humm PHP - <a href="https://www.davidesperalta.com" title="www.davidesperalta.com">www.davidesperalta.com</a></p>
 </footer>

</div> <!-- /container -->

<script type="text/javascript" src="<?= $viewsScriptsUrl ?>Thirdparty.min.js"></script>
