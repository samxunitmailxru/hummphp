<?php

/**
 * This file define some Humm PHP version related constants.
 *
 * To have an easy access place to update version information.
 *
 * @author D. Esperalta <info@davidesperalta.com>
 * @link https://www.davidesperalta.com/
 * @license https://www.gnu.org/licenses/gpl.html
 * @copyright (C)2019 Humm PHP - David Esperalta
 */

/**
 * Define the version of Humm PHP.
 */
\define('HUMM_VERSION_STRING', '2019.4 (01/22/2019)');

/**
 * Define the release date of Humm PHP.
 */
\define('HUMM_VERSION_RELEASE', '01/22/2019');
